#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QScrollArea>
#include <QLabel>
#include <QComboBox>
#include "portee.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    Portee *portee() const;
    QLabel *panneau() const;

    void setListeMelodies(QMap<QString, QString> liste);
    void setListeInterrogations(QMap<QString, QVariant> liste);

signals:
    void demarrerInterrogation(QString filename, QVariant type);

    void rejouer();

public slots:
    void alignerPortee(int instant);

    void activerRejeu();
    void desactiverRejeu();

    void aide();

    void testTermine();

private:
    QScrollArea *_scrollAreaPortee;
    Portee *_portee;
    QLabel *_panneau;

    QComboBox *_selecteurMelodies;
    QComboBox *_selecteurInterrogation;
    QAction *_actionRejeu;

private slots:
    void demarrer();
};

#endif // MAINWINDOW_H
