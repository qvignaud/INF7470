#include "portee.h"

Portee::Portee(QWidget *parent) : QWidget(parent), _noteEntree(0)
{
    _fonte.setFamily("FreeSerif");
    _fonte.setPointSizeF(espaceLignes * 6);
    _fontMetrics = new QFontMetricsF(_fonte, this);

    _texteClef.setText("𝄞");
    _texteClef.setTextFormat(Qt::PlainText);
    _textePortee.setText("𝄚");
    _textePortee.setTextFormat(Qt::PlainText);
    _texteLigne.setText("𝄖");
    _texteLigne.setTextFormat(Qt::PlainText);
    _texteBarre.setText("𝄀");
    _texteBarre.setTextFormat(Qt::PlainText);
    _texteBarreFinale.setText("𝄂");
    _texteBarreFinale.setTextFormat(Qt::PlainText);

    _fonteMesure = QFont(_fonte);
    _fonteMesure.setBold(true);
    _fonteMesure.setPointSizeF(espaceLignes * 3);

    _texteMesure4.setText("4");
    _texteMesure4.setTextFormat(Qt::PlainText);

    _texteNoire.setText("𝅘𝅥");
    _texteNoire.setTextFormat(Qt::PlainText);
    _texteSoupir.setText("𝄽");
    _texteSoupir.setTextFormat(Qt::PlainText);

    _fonteAlterations = QFont(_fonte);
    _fonteAlterations.setPointSizeF(_fonte.pointSizeF() / 3);
    _fontMetricsAlterations = new QFontMetricsF(_fonteAlterations, this);

    _texteDiese.setText("♯");
    _texteDiese.setTextFormat(Qt::PlainText);
    _texteBemol.setText("♭");
    _texteBemol.setTextFormat(Qt::PlainText);
    _texteBecarre.setText("♮");
    _texteBecarre.setTextFormat(Qt::PlainText);

    _fonteNomsNotes.setPointSizeF(espaceLignes);

    QRectF rectReference = _fontMetrics->boundingRect(_textePortee.text());
    _espace = rectReference.width();
    _hauteurLigne = rectReference.height() / 6;
    _margeSuperieure = _hauteurLigne * 4;

    setInstants(0);
    setFixedHeight(rectReference.height() * 2);

    _demandeEntree = false;
    setMouseTracking(true);
}

int Portee::instants() const
{
    return _instants;
}

void Portee::setInstants(int instants)
{
    _instants = instants;
    _sections = 2 + instants + instants/4;

    // Suppression de la section en trop.
    if ((instants-1)/4 != instants/4) --_sections;

    setFixedWidth(_sections * _espace);
    update();
}

int Portee::getInstantPosition(int instant) const
{
    return  _espace * (2 + instant + instant/4);
}

void Portee::vider()
{
    _notes.clear();
    update();
}

void Portee::ajouterNote(QMidiNote note)
{
    _notes.append(note);
    update();
}

void Portee::demanderEntree()
{
    _demandeEntree = true;
}

void Portee::arretEntree()
{
    _demandeEntree = false;
}

void Portee::mouseMoveEvent(QMouseEvent *event)
{
    if (!_demandeEntree) return;

    int xMin = getInstantPosition(_notes.size());
    int xMax = xMin + _espace;
    if (event->x() >= xMin && event->x() <= xMax) {
        int ligne = -(event->y() - _margeSuperieure - _hauteurLigne*6) / (_hauteurLigne / 2);
        if (ligne < 0 or ligne > 13) return;

        int note = 0;
        if (ligne % 7 <= 2) note = ((ligne % 7) * 2) + (12 * (ligne / 7));
        else note = ((ligne % 7) * 2) - 1 + (12 * (ligne / 7));

        if (event->x() > xMin + _espace / 2) {
            switch (note % 12) {
                case 2:
                case 4:
                    --note;
                    break;
                case 5:
                case 7:
                case 9:
                    ++note;
                    break;
            }
        }

        _noteEntree = QMidiNote(note + 60);
        update();
    }
}

void Portee::mousePressEvent(QMouseEvent *event)
{
    if (!_demandeEntree) return;

    if (event->button() == Qt::LeftButton and _noteEntree.note() >= 60) {
        emit entree(_noteEntree);
    }
}

void Portee::paintEvent(QPaintEvent*)
{
    QPainter painter(this);
    painter.setFont(_fonte);

    for (int section=0 ; section < _sections ; ++section) {
        painter.drawStaticText(QPointF(_espace * section, _margeSuperieure), _textePortee);
        if (section > 2 && (section-1) % 5 == 0)
            painter.drawStaticText(QPointF(_espace * section, _margeSuperieure), _texteBarre);
    }

    painter.drawStaticText(QPointF(0, _margeSuperieure), _texteBarre);
    painter.drawStaticText(QPointF(0, _margeSuperieure), _texteClef);
    painter.drawStaticText(QPointF(width() - _fontMetrics->boundingRect(_texteBarreFinale.text()).width(), _margeSuperieure), _texteBarreFinale);

    QList<QMidiNote> section;
    for (int instant=0 ; instant < _instants && instant < _notes.size() ; ++instant) {
        qreal position = _espace * (2 + instant + instant/4);
        dessinerNote(painter, _notes.at(instant), section, position);
        if (section.size() < 3) section.append(_notes.at(instant));
        else section.clear();
    }
    if (_demandeEntree and _noteEntree.note() >= 60) {
        painter.save();
        painter.setPen(Qt::blue);
        dessinerNote(painter, _noteEntree, section, getInstantPosition(_notes.size()));
        painter.restore();
    }

    painter.setFont(_fonteMesure);
    painter.drawStaticText(QPointF(_espace, _margeSuperieure + _hauteurLigne * 0.25), _texteMesure4);
    painter.drawStaticText(QPointF(_espace, _margeSuperieure + _hauteurLigne * 2.5), _texteMesure4);
}

void Portee::dessinerNote(QPainter &painter, QMidiNote note, QList<QMidiNote> prec, qreal position)
{
    //Écriture du nom.
    painter.setFont(_fonteNomsNotes);
    painter.drawText(QPointF(position, height()-espaceLignes), note.name());
    painter.setFont(_fonte);

    bool diese = false, bemol = false, becarre = false;

    // Détection hauteur et altération.
    switch (note.number()) {
        case 1:
        case 3:
            bemol = true;
            break;
        case 6:
        case 8:
        case 10:
            diese = true;
            break;
        default:
            break;
    }

    // Détection de normalisation.
    if (!bemol && !diese) {
        for (QMidiNote p: prec) {
            if (p.number() < 4 && p.number() + 1 == note.number())
                becarre = true;
            else if (p.number() >= 5 && p.number() - 1 == note.number())
                becarre = true;
        }
    }

    int hauteurBrute = note.note() - 60;
    int ligne = ((hauteurBrute + (hauteurBrute % 2) + ((hauteurBrute / 12) * 2)) / 2) - 1;
    qreal hauteur = _margeSuperieure - ((ligne * (_hauteurLigne/2)) - _hauteurLigne);

    // Ajout de ligne si sortant de la portée de base.
    if (ligne <= -1)
        painter.drawStaticText(QPointF(position - _espace/4, _margeSuperieure + _hauteurLigne * 3), _texteLigne);
    else if (ligne >= 11)
        painter.drawStaticText(QPointF(position - _espace/4, _margeSuperieure - _hauteurLigne * 3), _texteLigne);


    // Écriture de l'altération s'il y a lieu.
    painter.setFont(_fonteAlterations);
    qreal charHeight = _fontMetricsAlterations->height() * 1.5;
    if (bemol) {
        painter.drawStaticText(QPointF(position, hauteur + charHeight), _texteBemol);
        position += _fontMetricsAlterations->boundingRect(_texteBemol.text()).width();
    }
    if (diese) {
        painter.drawStaticText(QPointF(position, hauteur + charHeight), _texteDiese);
        position += _fontMetricsAlterations->boundingRect(_texteDiese.text()).width();
    }
    if (becarre) {
        painter.drawStaticText(QPointF(position, hauteur + charHeight), _texteBecarre);
        position += _fontMetricsAlterations->boundingRect(_texteBecarre.text()).width();
    }

    // Écriture de la noire.
    painter.setFont(_fonte);
    painter.drawStaticText(QPointF(position, hauteur), _texteNoire);
}
