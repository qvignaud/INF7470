#ifndef RENDUAUDIO_H
#define RENDUAUDIO_H

#include <QObject>
#include "QMidiNote.h"

class RenduAudio : public QObject
{
    Q_OBJECT
public:
    explicit RenduAudio(QObject *parent = nullptr);

signals:

public slots:

    void jouer(const QMidiNote &note) const;
};

#endif // RENDUAUDIO_H
