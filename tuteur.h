#ifndef TUTEUR_H
#define TUTEUR_H

#include <QObject>
#include "QMidiNote.h"

class Tuteur : public QObject
{
    Q_OBJECT
public:
    explicit Tuteur(QObject *parent = nullptr);

signals:
    void correct();
    void incorrect();
    void conseil(QString text);

public slots:
    void validation(QMidiNote entree, QMidiNote attendue);
};

#endif // TUTEUR_H
