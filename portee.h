#ifndef PORTEE_H
#define PORTEE_H

#include <QWidget>
#include <QPainter>
#include <QStaticText>
#include <QMouseEvent>
#include "QMidiNote.h"

class Portee : public QWidget
{
    Q_OBJECT
public:
    explicit Portee(QWidget *parent = nullptr);

    int instants() const;
    void setInstants(int instants);
    int getInstantPosition(int instant) const;

signals:
    void entree(QMidiNote note);

public slots:
    void vider();
    void ajouterNote(QMidiNote note);
    void demanderEntree();
    void arretEntree();

protected:
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *);

private:
    int _instants;
    int _sections;

    QFont _fonte;
    QFontMetricsF *_fontMetrics;
    QStaticText _texteClef;
    QStaticText _textePortee;
    QStaticText _texteLigne;
    QStaticText _texteBarre;
    QStaticText _texteBarreFinale;

    QFont _fonteMesure;
    QStaticText _texteMesure4;

    // Une seule durée actuellement représentée.
    // https://fr.wikipedia.org/wiki/Repr%C3%A9sentation_des_dur%C3%A9es_en_musique
    QStaticText _texteNoire;
    QStaticText _texteSoupir;
    // Altérations
    QFont _fonteAlterations;
    QFontMetricsF *_fontMetricsAlterations;
    QStaticText _texteDiese;
    QStaticText _texteBemol;
    QStaticText _texteBecarre;

    QFont _fonteNomsNotes;

    qreal _espace;
    qreal _hauteurLigne;
    qreal _margeSuperieure;

    QList<QMidiNote> _notes;

    bool _demandeEntree;
    QMidiNote _noteEntree;

    void dessinerNote(QPainter &painter, QMidiNote note, QList<QMidiNote> prec, qreal position);

    /// Espace entre les lignes de portée.
    const static int espaceLignes = 12;
};

#endif // PORTEE_H
