#ifndef LECTEURMIDI_H
#define LECTEURMIDI_H

#include <QObject>
#include "QMidiFile.h"
#include "QMidiNote.h"

class LecteurMidi : public QObject
{
    Q_OBJECT
public:
    explicit LecteurMidi(QObject *parent = nullptr);

    void setFichier(const QString &filename);

    uint intervalle() const;
    void setIntervalle(const uint &value);

    int pas() const;
    void setPas(int pas);
    int nombrePas() const;

    QMidiNote lastNote() const;

signals:
    void jouer(QMidiNote note);
    void pasJoue(int pas);
    void termine();

public slots:
    void redemarrer();
    void rejouer();
    void suivante();
    void suivantes(int nombre);

protected:
    void timerEvent(QTimerEvent *event);

private:
    QMidiFile midiFile;
    QList<QMidiNote> notes;
    int _pas;
    uint _intervalle;
};

#endif // LECTEURMIDI_H
