#include "lecteurmidi.h"

LecteurMidi::LecteurMidi(QObject *parent) : QObject(parent)
{
    _pas = 0;
    _intervalle = 500;
}

void LecteurMidi::setFichier(const QString &filename)
{
    midiFile.load(filename);

    auto events = midiFile.events(0);
    notes.clear();
    _pas = 0;
    for (auto event: events) {
        if (event->type() == QMidiEvent::NoteOn) {
            notes.append(event->note());
        }
    }
}

uint LecteurMidi::intervalle() const
{
    return _intervalle;
}

void LecteurMidi::setIntervalle(const uint &value)
{
    _intervalle = value;
}

void LecteurMidi::redemarrer()
{
    _pas = 0;
}

void LecteurMidi::rejouer()
{
    setPas(_pas-1);
    suivante();
}

void LecteurMidi::suivante()
{
    if (_pas < nombrePas()) {
        emit jouer(notes.at(_pas));
        emit pasJoue(_pas);
        ++_pas;
        if (_pas >= nombrePas())
            emit termine();
    }
}

void LecteurMidi::suivantes(int nombre)
{
    for (; nombre >= 1; --nombre) {
        startTimer(nombre * _intervalle, Qt::PreciseTimer);
    }
}

void LecteurMidi::timerEvent(QTimerEvent *event)
{
    killTimer(event->timerId());
    suivante();
}

int LecteurMidi::pas() const
{
    return _pas;
}

void LecteurMidi::setPas(int pas)
{
    if (pas >= 0 && pas < nombrePas())
        _pas = pas;
}

int LecteurMidi::nombrePas() const
{
    return notes.size();
}

QMidiNote LecteurMidi::lastNote() const
{
    return notes.at(_pas - 1);
}
