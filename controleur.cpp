#include "controleur.h"

Controleur::Controleur(LecteurMidi *lecteur, Portee *portee, Tuteur *tuteur, QObject *parent) : QObject(parent), _lecteur(lecteur), _portee(portee), _tuteur(tuteur)
{
    _mode = Mode::TranscriptionDirecte;
    _type = Type::JeuSimple;
    connect(lecteur, &LecteurMidi::jouer, this, &Controleur::noteJouee);
    connect(portee, &Portee::entree, this, &Controleur::noteEntree);
    connect(tuteur, &Tuteur::correct, this, &Controleur::noteCorrecte);
    connect(tuteur, &Tuteur::incorrect, this, &Controleur::noteIncorrecte);
}

Controleur::Mode Controleur::mode() const
{
    return _mode;
}

Controleur::Type Controleur::type() const
{
    return _type;
}

void Controleur::demarrer(QString fichier, QVariant type)
{
    setModeTranscriptionDirecte();
    _type = type.value<Type>();

    _lecteur->setFichier(fichier);
    _portee->vider();
    _portee->setInstants(_lecteur->nombrePas());

    if (_type == Type::JeuSimple) {
        _lecteur->suivantes(_lecteur->nombrePas());
    }
    else if (_type == Type::QuatrePuisInterrogation) {
        _lecteur->suivantes(4);
        startTimer(_lecteur->intervalle() * 5, Qt::PreciseTimer);
    }

    emit testDemarre();
}

void Controleur::setMode(const Mode &mode)
{
    switch (mode) {
    case Mode::Interrogation:
        setModeInterrogation();
        break;
    case Mode::TranscriptionDirecte:
        setModeTranscriptionDirecte();
        break;
    }
}

void Controleur::setModeTranscriptionDirecte()
{
    _mode = Mode::TranscriptionDirecte;
    _portee->arretEntree();
    emit phaseJeu();
}

void Controleur::setModeInterrogation()
{
    _mode = Mode::Interrogation;
    _portee->demanderEntree();
    emit phaseInterrogation();
}

void Controleur::timerEvent(QTimerEvent *event)
{
    killTimer(event->timerId());

    if (_type != Type::JeuSimple) {
        setModeInterrogation();
        _lecteur->suivante();
    }
}

void Controleur::noteJouee(QMidiNote note)
{
    if (_mode == Mode::TranscriptionDirecte) {
        _portee->ajouterNote(note);
    }
}

void Controleur::noteEntree(QMidiNote note)
{
    _tuteur->validation(note, _lecteur->lastNote());
}

void Controleur::noteCorrecte()
{
    _portee->ajouterNote(_lecteur->lastNote());

    if (_lecteur->pas() <= _lecteur->nombrePas()-1)
        _lecteur->suivante();
    else
        emit testTermine();
}

void Controleur::noteIncorrecte()
{
    _lecteur->rejouer();
}
