#include "tuteur.h"

Tuteur::Tuteur(QObject *parent) : QObject(parent)
{

}

void Tuteur::validation(QMidiNote entree, QMidiNote attendue)
{
    if (entree.note() == attendue.note()) {
        emit correct();
        emit conseil("Transcription correcte !");
    }
    else {
        emit incorrect();

        if (entree.number() == attendue.number()) {
            // Octave décalé.
            emit conseil("Il s'agit de la bonne note ! Mais pas sur la bonne octave…");
        }
        else if (entree.note() == attendue.note()-1) {
            // Un demi-ton en dessous…
            emit conseil("Presque! Un demi-ton plus haut.");
        }
        else if (entree.note() == attendue.note()+1) {
            // Un demi-ton en dessous…
            emit conseil("Presque! Un demi-ton plus bas.");
        }
        else if (entree.note() == attendue.note()+7 or entree.note() == attendue.note()-7){
            // Une  quinte, 5ème note sur une gamme ou 7 demi-tons entre deux notes
            emit conseil("Il y a un décalage d'une quinte juste, soit de cinq notes ou sept demi-tons.");
        }
        else if (entree.note() == attendue.note()+4 or entree.note() == attendue.note()-4){
            // Une tierce, l'autre élément de l'accord parfait avec la quinte
            emit conseil("Il y a un décalage d'une tierce, soit de deux notes ou trois demi-tons.");
        }
        else {
            emit conseil("Désolé, la note transcrite n'a pas de correspondance avec celle jouée.");
        }
    }
}
