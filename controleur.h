#ifndef CONTROLEUR_H
#define CONTROLEUR_H

#include <QObject>
#include "lecteurmidi.h"
#include "portee.h"
#include "tuteur.h"

class Controleur : public QObject
{
    Q_OBJECT
public:
    explicit Controleur(LecteurMidi *lecteur, Portee *portee, Tuteur *tuteur, QObject *parent = nullptr);

    enum Mode {
        TranscriptionDirecte,
        Interrogation
    };

    enum Type {
        JeuSimple,
        QuatrePuisInterrogation
    };
    Q_ENUM(Type)

    Mode mode() const;

    Type type() const;

    void demarrer(QString fichier, QVariant type);

signals:
    void testDemarre();
    void phaseJeu();
    void phaseInterrogation();
    void testTermine();

public slots:
    void setMode(const Mode &mode);
    void setModeTranscriptionDirecte();
    void setModeInterrogation();

protected:
    void timerEvent(QTimerEvent *event);

private:
    Mode _mode;
    Type _type;
    LecteurMidi *_lecteur;
    Portee *_portee;
    Tuteur *_tuteur;

private slots:
    void noteJouee(QMidiNote note);
    void noteEntree(QMidiNote note);

    void noteCorrecte();
    void noteIncorrecte();
};

#endif // CONTROLEUR_H
