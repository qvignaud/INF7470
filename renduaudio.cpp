#include "renduaudio.h"
#include <QSound>

RenduAudio::RenduAudio(QObject *parent) : QObject(parent)
{

}

void RenduAudio::jouer(const QMidiNote &note) const
{
    // Numéro du fichier.
    int num = note.note() - 59;
    QString fileName = "note" + QString::number(num) + ".wav";
    QSound::play(":/notes/" + fileName);
}
