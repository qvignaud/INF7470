# Système Tutoriel Intelligent pour transcription musicale

Ce programme de STI a été réalisé dans le cadre du cours INF7470 offert à l'UQÀM en session d'automne 2018 par monsieur Roger Nkambou.

## Compilation

Le projet requiert la bibliothèque Qt version 5.11 ou supérieure.

Téléchargez le projet avec la commande `git clone --recurse-submodules git@gitlab.com:qvignaud/INF7470.git`.
Dans le répertoire du projet, exécutez `qmake` suivi de `make`.

## Organisation

Les différentes classes effectuent les tâches comme suit :
- Controleur : gère le contôle général des interactions entre le lecteur musical, le tuteur, et l'affichage graphique ;
- LecteurMidi : effectue la lecture des pistes MIDI ;
- MainWindow : fenêtre d'affichage principale ;
- Portee : élément graphique d'affichage et d'interaction utilisateur de la portée ;
- RenduAudio : effectue le rendu audio des notes ;
- Tuteur : contient le système de tutorat.

