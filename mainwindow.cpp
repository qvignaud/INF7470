#include "mainwindow.h"
#include <QBoxLayout>
#include <QToolBar>
#include <QStatusBar>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    QWidget *mainWidget = new QWidget(this);
    setCentralWidget(mainWidget);

    QBoxLayout *mainLayout = new QBoxLayout(QBoxLayout::TopToBottom, mainWidget);

    _scrollAreaPortee = new QScrollArea(this);
    _portee = new Portee(this);
    _scrollAreaPortee->setWidget(_portee);
    _scrollAreaPortee->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    _scrollAreaPortee->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _scrollAreaPortee->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    mainLayout->addWidget(_scrollAreaPortee);

    _panneau = new QLabel(this);
    _panneau->setFrameShape(_scrollAreaPortee->frameShape());
    _panneau->setMinimumHeight(150);
    mainLayout->addWidget(_panneau);

    QToolBar *toolBar = addToolBar("Actions");
    toolBar->setMovable(false);

    _selecteurMelodies = new QComboBox(this);
    toolBar->addWidget(_selecteurMelodies);

    _selecteurInterrogation = new QComboBox(this);
    toolBar->addWidget(_selecteurInterrogation);

    toolBar->addAction(QIcon::fromTheme("media-playback-start"), "Démarrer", this, &MainWindow::demarrer);

    toolBar->addSeparator();
    _actionRejeu = toolBar->addAction(QIcon::fromTheme("view-refresh"), "Rejouer", this, &MainWindow::rejouer);
    _actionRejeu->setEnabled(false);

    toolBar->addSeparator();
    toolBar->addAction(QIcon::fromTheme("help"), "Aide", this, &MainWindow::aide);

    toolBar->addSeparator();
    toolBar->addAction(QIcon::fromTheme("application-exit"), "Quitter", this, &MainWindow::close);

    statusBar()->showMessage("Prêt");

    setMinimumSize(640, 480);
}

MainWindow::~MainWindow()
{
    delete _portee;
    delete _panneau;
}

Portee *MainWindow::portee() const
{
    return _portee;
}

QLabel *MainWindow::panneau() const
{
    return _panneau;
}

void MainWindow::setListeMelodies(QMap<QString, QString> liste)
{
    _selecteurMelodies->clear();
    for (auto it = liste.begin() ; it != liste.end() ; ++it) {
        _selecteurMelodies->addItem(it.value(), it.key());
    }
}

void MainWindow::setListeInterrogations(QMap<QString, QVariant> liste)
{
    _selecteurInterrogation->clear();
    for (auto it = liste.begin() ; it != liste.end() ; ++it) {
        _selecteurInterrogation->addItem(it.key(), it.value());
    }
}

void MainWindow::alignerPortee(int instant)
{
    _scrollAreaPortee->ensureVisible(_portee->getInstantPosition(instant), 0, 200);
}

void MainWindow::activerRejeu()
{
    _actionRejeu->setEnabled(true);
}

void MainWindow::desactiverRejeu()
{
    _actionRejeu->setEnabled(false);
}

void MainWindow::aide()
{
    QMessageBox::information(this, "Information",
                             "Pour vous exercer en premier lieu, effectuez une transcription des notes par demi-tons ascendants de la portée.<br><br>Les quatre premières notes sont jouées et écrites, et à partir de la cinquième vous devez indiquer la transcription correcte.<br><br>Effectuez ensuite la transcription de la mélodie de votre choix de la même manière que précédemment. Vous pouvez réécouter la note à transcrire autant que voulu avec le bouton Rejouer.<br><br>Vous pouvez écouter une mélodie au complet et en voir la transcription intégrale au moyen du mode d'écoute."
                             );
}

void MainWindow::testTermine()
{
    QMessageBox::information(this, "Terminé !",
                             "La transcription est terminée !"
                             );
}

void MainWindow::demarrer()
{
    emit demarrerInterrogation(_selecteurMelodies->currentData().toString(), _selecteurInterrogation->currentData());
}
