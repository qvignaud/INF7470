#include "mainwindow.h"
#include <QApplication>
#include <QFontDatabase>
#include "lecteurmidi.h"
#include "renduaudio.h"
#include "tuteur.h"
#include "controleur.h"

QMap<QString, QString> melodies()
{
    QMap<QString, QString> melos;

    melos.insert(":/melodies/montant.midi", "Demi-tons ascendants");
    melos.insert(":/melodies/sapin.midi", "Mon beau sapin");
    melos.insert(":/melodies/saintenuit.midi", "Sainte nuit");
    melos.insert(":/melodies/vivelevent.midi", "Vive le vent");
    //melos.insert("/tmp/test.midi", "(Dev) Test");

    return melos;
}

QMap<QString, QVariant> interrogationTypes()
{
    QMap<QString, QVariant> inters;

    inters.insert("Écoute", Controleur::Type::JeuSimple);
    inters.insert("Transcription une à une", Controleur::Type::QuatrePuisInterrogation);

    return inters;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFontDatabase::addApplicationFont(":/fonts/FreeSerif.ttf");

    RenduAudio ra;
    LecteurMidi lm;

    QObject::connect(&lm, &LecteurMidi::jouer, &ra, &RenduAudio::jouer);

    MainWindow w;
    w.setListeMelodies(melodies());
    w.setListeInterrogations(interrogationTypes());

    QObject::connect(&lm, &LecteurMidi::pasJoue, &w, &MainWindow::alignerPortee);
    QObject::connect(&w, &MainWindow::rejouer, &lm, &LecteurMidi::rejouer);

    Tuteur t;

    QObject::connect(&t, &Tuteur::conseil, w.panneau(), &QLabel::setText);

    Controleur c(&lm, w.portee(), &t);

    QObject::connect(&w, &MainWindow::demarrerInterrogation, &c, &Controleur::demarrer);
    QObject::connect(&c, &Controleur::phaseJeu, &w, &MainWindow::desactiverRejeu);
    QObject::connect(&c, &Controleur::phaseInterrogation, &w, &MainWindow::activerRejeu);
    QObject::connect(&c, &Controleur::testTermine, &w, &MainWindow::testTermine);

    if (QSysInfo::currentCpuArchitecture().compare("x86-JS") == 0) {
        w.setWindowFlag(Qt::FramelessWindowHint);
        w.showFullScreen();
    }
    else
        w.show();

    w.aide();

    return a.exec();
}
